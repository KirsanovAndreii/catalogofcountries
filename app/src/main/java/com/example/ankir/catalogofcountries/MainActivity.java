package com.example.ankir.catalogofcountries;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    public static List<ResponseCountries> bigList = new ArrayList<>();
    public static final int KEY_Countries = 3;
    public static final String FILE_NAME = "my_file";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);




        if (fileExistance(FILE_NAME)) {
            Toast.makeText(MainActivity.this, "Disk", Toast.LENGTH_SHORT).show();
            readData();

        } else {
            Toast.makeText(MainActivity.this, "Internet", Toast.LENGTH_SHORT).show();
            getData();
        }
    }



    private void getData() {
        Retrofit.getCountries(new Callback<List<ResponseCountries>>() {
            @Override
            public void success(List<ResponseCountries> responseCountries, Response response) {
                updateUI(responseCountries);
                Toast.makeText(MainActivity.this, "ok", Toast.LENGTH_SHORT).show();
                saveData(responseCountries);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void saveData(List<ResponseCountries> responseCountries) {
        FileOutputStream fos = null; //open stream for writing data to file_name file
        try {
            fos = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            String json = new Gson().toJson(responseCountries); //generate json
            fos.write(json.getBytes()); //write bites array
            fos.close(); //important: close stream
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }


    private void readData() {

        StringBuilder json = new StringBuilder();
        try {
            FileInputStream fis = openFileInput(FILE_NAME);//open stream for reading data from file_name file
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis)); //create reader from stream
            String line;
            while ((line = reader.readLine()) != null) { // get text line by line, while text presen
                json.append(line); // save text line to StringBuilder
            }
            fis.close(); //important: close reader
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<ArrayList<ResponseCountries>>(){}.getType();
        List<ResponseCountries> responseCountries = new Gson().fromJson(json.toString(), listType);
        updateUI(responseCountries);


    }

    private void updateUI(List<ResponseCountries> responseCountries) {
        bigList.addAll(responseCountries);

    }
    @OnClick(R.id.am_region)
    void onRegionClick()

    {
        Intent intent = new Intent(MainActivity.this, Main2ActivityCountry.class);
        intent.putExtra("KEY", 1);
        startActivity(intent);
    }

    @OnClick(R.id.am_subregion)
    void onSubregionClick()

    {
        Intent intent = new Intent(MainActivity.this, Main2ActivityCountry.class);
        intent.putExtra("KEY", 2);
        startActivity(intent);
    }

    @OnClick(R.id.am_country)
    void onCountryClick()

    {
        Intent intent = new Intent(MainActivity.this, Main2ActivityCountry.class);
         intent.putExtra("KEY", 3);
        startActivity(intent);
    }

    private boolean fileExistance(String fname) {
        File file = getBaseContext().getFileStreamPath(fname);
        return file.exists();
    }
}
