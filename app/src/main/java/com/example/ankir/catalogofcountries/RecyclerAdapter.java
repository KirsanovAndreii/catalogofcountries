package com.example.ankir.catalogofcountries;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;



/**
 * Created by ankir on 01.06.2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
Context context;
    private List<String> listCurrent;

    public RecyclerAdapter(Context context, List<String> listCurrent) {
        this.context = context;
        this.listCurrent = listCurrent;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        String org = listCurrent.get(position);
        viewHolder.item.setText(org);
    }

    @Override
    public int getItemCount() {
        return listCurrent.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
private TextView item;

    public ViewHolder(View itemViev) {
        super(itemViev);
        item = (TextView) itemViev.findViewById(R.id.item);

    }


    }

}
