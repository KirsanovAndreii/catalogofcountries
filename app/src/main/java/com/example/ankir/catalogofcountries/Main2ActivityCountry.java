package com.example.ankir.catalogofcountries;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class Main2ActivityCountry extends AppCompatActivity {

    RecyclerView rList;
    List<String> listCurrent ;
    RecyclerAdapter adapter;
    private int key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2_country);
Intent intent = getIntent();
        key = intent.getIntExtra("KEY",0);

        listCurrent = createList();
        rList = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rList.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter(this, listCurrent);
        rList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    List<String> createList(){
        if (key == 1) {
            listCurrent = new ArrayList<>();
            for (int i = 0; i < MainActivity.bigList.size(); i++) {
                listCurrent.add(MainActivity.bigList.get(i).region);
            }
        }

        if (key == 2) {
            listCurrent = new ArrayList<>();
            for (int i = 0; i < MainActivity.bigList.size(); i++) {
                listCurrent.add(MainActivity.bigList.get(i).subregion);
            }
        }



        if (key == 3) {
            listCurrent = new ArrayList<>();
            for (int i = 0; i < MainActivity.bigList.size(); i++) {
                listCurrent.add(MainActivity.bigList.get(i).name);
            }
        }
return listCurrent;
    }
}
